@extends('layouts.master')

@section('content')
<section class="mx-3 pt-3">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Edit Pertanyaan {{$show->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan/{{$show->id}}" method="POST">
            @csrf
            @method("PUT")
          <div class="card-body">
            <div class="form-group">
              <label for="judul">Judul</label>
              <input type="text" class="form-control" id="judul" name="judul" placeholder="Enter Judul" value="{{old("judul", $show->judul)}}">

              @error('judul')
                <div class="alert alert-danger mt-2">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="isi">Isi</label>
              <input type="text" class="form-control" id="isi" name="isi" placeholder="Enter Isi" value="{{old("isi", $show->isi)}}">
              @error('isi')
                <div class="alert alert-danger mt-2">{{ $message }}</div>
              @enderror
            </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
            <a class="btn btn-primary ml-2" href="/pertanyaan" role="button">Go Back</a>
          </div>
        </form>
      </div>
</section>
@endsection