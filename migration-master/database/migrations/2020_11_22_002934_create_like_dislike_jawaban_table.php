<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikeJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_jawaban', function (Blueprint $table) {
            $table->bigInteger("poin");
            $table->unsignedBigInteger('profil_id');
            $table->unsignedBigInteger('jawaban_id');
            $table->primary(['profil_id', 'jawaban_id']);

            $table->foreign('profil_id')->references('id')->on('profil');
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_jawaban');

        Schema::dropIfExists('like_dislike_jawaban', function (Blueprint $table) {
            $table->dropForeign('profil_id');
            $table->dropForeign('jawaban_id');
            $table->dropColumn('profil_id');
            $table->dropColumn('jawaban_id');
        });
    }
}
