<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create() {
        return view("tampilan.create");
    }

    public function store(Request $request) {
        $request->validate([
            "judul" => 'bail|required|unique:pertanyaan',
            "isi" => 'required',
        ]);

        $query = DB::table("pertanyaan")->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect("/pertanyaan")->with("success", "Pertanyaan berhasil disimpan");
    }

    public function index() {
        $display = DB::table("pertanyaan")->get(); // kalau di sql-> SELECT * FROM nama_table
        return view("tampilan.index", compact("display"));
    }

    public function show($pertanyaan_id) {
        $show = DB::table("pertanyaan")->where("id", $pertanyaan_id)->first();
        return view("tampilan.show", compact("show"));
    }

    public function edit($pertanyaan_id) {
        $show = DB::table("pertanyaan")->where("id", $pertanyaan_id)->first();
        return view("tampilan.edit", compact("show"));
    }

    public function update($pertanyaan_id, Request $request) {
        $request->validate([
            "judul" => 'bail|required|unique:pertanyaan',
            "isi" => 'required',
        ]);

        $update = DB::table("pertanyaan")
                    ->where("id", $pertanyaan_id)
                    ->update([
                        "judul" => $request["judul"],
                        "isi" => $request["isi"]
                    ]);
        
        return redirect("/pertanyaan")->with("success", "Pertanyaan berhasil di-update");
    }

    public function destroy($pertanyaan_id) {
        $destroy = DB::table("pertanyaan")->where("id", $pertanyaan_id)->delete();
        return redirect("pertanyaan")->with("success", "Pertanyaan berhasil di-delete");
    }
}
